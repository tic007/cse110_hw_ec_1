package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class FacadeFactory {
	
	//static Book bk;
	//static Member mem;
	public static LibraryFacade createFacade(Library lib) {
		return new LibraryFacade(lib.book, lib.member);
	}
	
}
