package edu.ucsd.cse110.library;

import java.time.LocalDate;

import edu.ucsd.cse110.library.rules.RuleObjectBasedLateFeesStrategy;

public class LibraryFacade {
	RuleObjectBasedLateFeesStrategy ro = new RuleObjectBasedLateFeesStrategy();
	Book bk;
	Member mem;
	
	public LibraryFacade( Book book, Member mem) {
		bk = book;
		this.mem = mem;
	}
	
	void checkoutPublication(Member mem, Publication pub) {
		LocalDate checkoutDate = LocalDate.now();
		pub.checkout(mem, checkoutDate);
		
	}
	
	void returnPublication(Publication pub) {
		pub.pubReturn(LocalDate.now());
	}
	
	double getFee(Member mem) {
		return 0;
	}
	
	boolean hasFee(Member mem) {
		return true;
	}
}
