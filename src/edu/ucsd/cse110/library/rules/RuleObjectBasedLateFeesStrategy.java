package edu.ucsd.cse110.library.rules;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import edu.ucsd.cse110.library.LateFeesStrategy;
import edu.ucsd.cse110.library.Publication;

public class RuleObjectBasedLateFeesStrategy implements LateFeesStrategy {

	List<IRuleObject> libraryRules;
	
	public RuleObjectBasedLateFeesStrategy() {
		libraryRules = new LinkedList<IRuleObject>();
		// During the school year students can keep the books for 2 weeks, 
		// pay 1 dollar per day when are late, 
		// and are never charged more than 10 dollars per month
		libraryRules.add(new RuleObject(new StudentLateSchoolYearAssessor(), new ChargePerDayFeeWithMaxFeeAction(1.00, 14, 10.00)));
		// During summer students can keep the books for 1 month, 
		// pay 1 dollar per day when are late, 
		// and are never charged more than 10 dollars per month	
		libraryRules.add(new RuleObject(new StudentLateSummerAssessor(), new ChargePerDayFeeWithMaxFeeAction(1.00, 30, 10.00)));
		// Teachers can keep books for 2 weeks and are charged 1 dollar per day when late 
		libraryRules.add(new RuleObject(new TeacherLateAssessor(), new ChargePerDayFeeAction(1.00, 14)));
		// Other customers can keep books for 1 week and are charged 1.5 dollars per day when late 
		libraryRules.add(new RuleObject(new OthersLateAssessor(), new ChargePerDayFeeAction(1.50, 7)));
	}

	@Override
	public void applyRule(Publication publication, LocalDate returnDate) {
		Properties prop = new Properties(publication, returnDate);
		for (IRuleObject rule : libraryRules) {
			rule.checkRule(prop);
			//Report possible errors
			if(rule.getResults().hasErrors()) {
				if (rule.getResults().getAssessorResults()!=null)
					System.err.println(rule.getResults().getAssessorResults());
				if (rule.getResults().getActionResults()!=null)
					System.err.println(rule.getResults().getActionResults());
			}
		}
	}
}
